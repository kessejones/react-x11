class Window {
    constructor(props={}, client){
        const {events, ...attrs} = {...props};
        this.window = client.createWindow({...attrs});
        this._initEvents(events);

        this.window.map();
    }      

    getWindow(){
        return this.window;
    }

    _initEvents(events){
        if(events){
            for(let eventName in events){
                this.window.on(eventName, events[eventName]);
            }
        }
    }
}

export default Window;