import ntk from 'ntk';

type WindowNtk = {};
type AppNtk = {};
type EventsWindowNtk = {};

class Window {
    constructor(props: Object, root: AppNtk);  

    getWindow(): WindowNtk;

    setWindow(window: WindowNtk): void;

    _initEvents(events: EventsWindowNtk): void;
}

export default Window;