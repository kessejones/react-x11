class Root {
    constructor(props = {}, client){
        const {events, ...attrs} = {...props};
        this.window = client.rootWindow();
        this._initEvents(events);    
    }      

    getWindow(){
        return this.window;
    }

    _initEvents(events){
        if(events){
            for(let eventName in events){
                this.window.on(eventName, events[eventName]);
            }
        }
    }
}

export default Root;