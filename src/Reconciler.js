import Reconciler from 'react-reconciler';
import ntk from 'ntk';

import Window from './components/Window';
import Root from './components/Root';

const Renderer = new Reconciler({
    now: () => Date.now(),

    supportsMutation: true,

    appendInitialChild(parentInstance, child){
        console.log('APPEND INITIAL CHILD');
        const parent_window = parentInstance.getWindow();
        const child_window = child.getWindow();

        if(parentInstance.__children)
            parentInstance.__children.push(child);
        else
            parentInstance.__children = [child];

        child_window.reparentTo(parent_window, child_window.x, child_window.y);
    },
    
    finalizeInitialChildren(instance, type, props, rootContainerInstance){
        return true;
    },

    appendChildToContainer(container, child){
        console.log('APPEND TO CONTAINER');
        // console.log(container);
        // console.log(child);
        // parentInstance.__children.push(child);
        // const parent_window = container.getWindow();
        // const child_window = child.getWindow();
        // child_window.reparentTo(parent_window, child_window.x, child_window.y);
    },
   
    appendChild(parentInstance, child){
        console.log('APPEND CHILD');

        const parent_window = parentInstance.getWindow();
        const child_window = child.getWindow();

        if(parentInstance.__children)
            parentInstance.__children.push(child);
        else
            parentInstance.__children = [child];

        child_window.reparentTo(parent_window, child_window.x, child_window.y);

    },

    createInstance(type, props, rootContainerInstance, hostContext, internalInstanceHandle){
        let instance = null;

        switch(type){
            case 'window':
                instance = new Window(props, rootContainerInstance);
                instance.getWindow().map();
                break;
            case 'root':
                instance = new Root(props, rootContainerInstance);
                break;
        }

        return instance;
    },

    commitMount(instance, type, newProps, internalInstaceHandle){
        if(type === 'window'){
            instance.getWindow().map();
        }
    },

    getPublicInstance(instance) {
        return instance.getWindow();
    },


    getRootHostContext(rootContainerInstance){
        return {
            
        };
    },

    prepareForCommit() {

    },

    resetAfterCommit() {

    },

    prepareForCommit(){

    },

    resetAfterCommit(){

    },

    prepareUpdate(instance, type, oldProps, newProps, rootContainerInstance, hostContext){
        switch(type){
            case 'window':
                const window = instance.getWindow();

                if(window.x !== newProps.x || window.y !== newProps.y){
                    window.move(newProps.x, newProps.y);
                }

                break;
        }
        return true;
    },
    
    commitUpdate(instance, updatePayload, type, oldProps, newProps, internalInstanceHandle){
        return true;
    },

    getChildHostContext(parentHostContext, type, rootContainerInstance, a, b, c){
        return {};
    },
    
    shouldSetTextContent(type, props){
        return false;

        // if (typeof props.children === 'string') {
        //     return true;
        // }
        // return false;
    },

    removeChild(parentInstance, child){
        child.getWindow().distroy();
    }
});


let app_cache = null;
let root_container = null;
const ReactX11 = {
    render(element, callback, container){
        if(!container){
            if(app_cache){
                return ReactX11.render(element, callback, app_cache);
            }
            ntk.createClient((err, app) => {
                app_cache = app;
                ReactX11.render(element, callback, app_cache);
            });
            return;
        }

        if(!root_container)
            root_container = Renderer.createContainer(container);
        Renderer.updateContainer(element, root_container, null);
        // callback && callback();
    },

    unmountComponentAtNode(container) {

    }
}

export default ReactX11;