import ReactX11 from '../../src/Reconciler';
import React, { Component } from 'react';

import Frame from './lib/Frame'

class App extends Component {

    constructor(){
        super();

        this.state = {
            windows: []
        }

        this.MapRequest = this.MapRequest.bind(this);
    }

    parent_expose(ev){
        const ctx = ev.window.getContext('2d');
        ctx.fillStyle = 'red';
        ctx.fillRect(0, 0, ctx.width, ctx.height);
    }
    
    child_expose(ev){
        const ctx = ev.window.getContext('2d');
        ctx.fillStyle = 'green';
        ctx.fillRect(0, 0, ctx.width, ctx.height);
    }

    renderWindows(){
        return this.state.windows.map(win => win);
    }

    componentDidMount(){
        const frame = (
            <Frame key={this.state.windows.length} window={{x: 0, y: 0, width: 100, height: 100}}/>
        );

        const wins = this.state.windows;
        wins.push(frame);

        this.setState({
            windows: wins
        });
    }

    MapRequest(event){
        console.log(event);

        const frame = (
            <Frame window={event.window}/>
        );

        const wins = this.state.windows;
        wins.push(frame);

        this.setState({
            windows: wins
        });
    }

    render(){
        return (
            // <root events={{map_request: this.MapRequest }}>
            <window events={{expose: this.child_expose}} x={0} y={0} width={200} height={200}>
                {this.renderWindows()}
            </window>
            // </root>
        );
    }
}

ReactX11.render(<App />);