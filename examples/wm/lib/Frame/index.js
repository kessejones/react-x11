import React, { Component } from 'react';

class Frame extends Component {

    constructor(props){
        super(props);
        this.mouse_pressed = false;

        this.state = {
            event: {
                mouse_pressed: false,
                x: 0,
                y: 0,
            },
            window: {
                id: this.props.window.id,
                x: this.props.window.x,
                y: this.props.window.y,
                width: this.props.window.width,
                height: this.props.window.height
            }
        }

        this.frame_mouse_move = this.frame_mouse_move.bind(this);
    }

    frame_expose(ev){
        const ctx = ev.window.getContext('2d');
        ctx.fillStyle = 'red';
        ctx.fillRect(0, 0, ctx.width, ctx.height);
	}
	
	frame_mouse_move(event){
        // console.log(event);

        if(event.buttons === 272){
            // console.log(event);
            const window_props = this.state.event;
            const window = event.window;

            let x = 0;
            let y = 0;

            x = (window.x - window_props.x) + event.x;
            y = (window.y - window_props.y) + event.y;

            console.log('X1: ', x);
            console.log('X2: ', window_props.x);
            console.log('X3: ', event.x);
            console.log('Y1: ', y);
            console.log('Y2: ', window_props.y);
            console.log('Y3: ', event.y);

            // window_props.x = x;
            // window_props.y = y;

            // this.setState({
            //     window: window_props
            // });
        }
    }
    
    frame_mouse_down(event){

        // console.log(event);

        this.setState({
            event: {
                x: event.x,
                y: event.y,
                mouse_pressed: true,
            }
        });
        // console.log(event);
    }
    
    frame_mouse_up(event){
        // this.mouse_pressed = false;
        this.setState({
            event: {
                x: 0,
                y: 0,
                mouse_pressed: false
            }
        });
    }

    render(){
        console.log(this.state);
        const {id, ...attrs} = this.state.window;
        return (
            <window 
                events={{expose: this.frame_expose, mousemove: this.frame_mouse_move, mousedown: this.frame_mouse_down, mouseup: this.frame_mouse_up}} 
                    {...attrs}
                >
					{/* <window 
						events={{mousemove: events => {}}} 
						id={this.props.window.id} 
						x={this.props.window.x + 5} 
						y={this.props.window.y+10} 
						width={this.props.window.width} 
						height={this.props.window.height}/> */}
            </window>
        );
    }
}

export default Frame;