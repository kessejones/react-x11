import ReactX11 from '../../src/Reconciler';
import React, { Component } from 'react';

class App extends Component {
    parent_expose(ev){
        const ctx = ev.window.getContext('2d');
        ctx.fillStyle = 'red';
        ctx.fillRect(0, 0, ctx.width, ctx.height);
    }
    
    child_expose(ev){
        console.log(ev.window);
        const ctx = ev.window.getContext('2d');
        ctx.fillStyle = 'green';
        ctx.fillRect(0, 0, ctx.width, ctx.height);
    }

    render(){
        return (
            <window events={{'expose': this.parent_expose}} x={0} y={0} width={500} height={100}>
                <window events={{'expose': this.child_expose}} x={10} y={10} width={10} height={10} />
            </window>
        );
    }
}

ReactX11.render(<App />);